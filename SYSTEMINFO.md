# Owasp_zap

Vendor: Owasp
Homepage: https://owasp.org/

Product: Zap
Product Page: https://owasp.org/www-chapter-dorset/assets/presentations/2020-01/20200120-OWASPDorset-ZAP-DanielW.pdf

## Introduction
We classify Owasp Zap into the Security (SASE) domain as Owasp Zap provides Security solutions. 

"The world’s most popular free web security tool, actively maintained by a dedicated international team of volunteers." 

## Why Integrate
The Owasp Zap adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Owasp Zap to offer security and trust information. 

With this adapter you have the ability to perform operations with Owasp Zap such as:

- Scanners
- Policies
- Alerts

## Additional Product Documentation
The [Zap API Catalogue](https://www.zaproxy.org/docs/api/#api-catalogue)
