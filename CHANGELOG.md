
## 0.6.4 [10-15-2024]

* Changes made at 2024.10.14_20:52PM

See merge request itentialopensource/adapters/adapter-owasp_zap!17

---

## 0.6.3 [09-14-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-owasp_zap!15

---

## 0.6.2 [08-14-2024]

* Changes made at 2024.08.14_19:10PM

See merge request itentialopensource/adapters/adapter-owasp_zap!14

---

## 0.6.1 [08-07-2024]

* Changes made at 2024.08.06_20:24PM

See merge request itentialopensource/adapters/adapter-owasp_zap!13

---

## 0.6.0 [05-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-owasp_zap!12

---

## 0.5.3 [03-27-2024]

* Changes made at 2024.03.27_13:49PM

See merge request itentialopensource/adapters/security/adapter-owasp_zap!11

---

## 0.5.2 [03-11-2024]

* Changes made at 2024.03.11_16:18PM

See merge request itentialopensource/adapters/security/adapter-owasp_zap!10

---

## 0.5.1 [02-27-2024]

* Changes made at 2024.02.27_11:54AM

See merge request itentialopensource/adapters/security/adapter-owasp_zap!9

---

## 0.5.0 [01-05-2024]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/security/adapter-owasp_zap!8

---

## 0.4.0 [01-04-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-owasp_zap!6

---

## 0.3.0 [03-21-2023]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-owasp_zap!5

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-owasp_zap!5

---

## 0.1.3 [03-11-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/security/adapter-owasp_zap!4

---

## 0.1.2 [08-17-2020] & 0.1.1 [08-14-2020]

- Adding pipeline and lint files

See merge request itentialopensource/adapters/security/adapter-owasp_zap!3

---
