## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Owasp Zap. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Owasp Zap.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Owasp_zap. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getJSONSpiderActionScan(url, callback)</td>
    <td style="padding:15px"># To start the Ajax Spider</td>
    <td style="padding:15px">{base_path}/{version}/JSON/spider/action/scan/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONContextActionNewContext(contextName, callback)</td>
    <td style="padding:15px">Create a new context for auth.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/context/action/newContext/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONContextViewContextList(callback)</td>
    <td style="padding:15px">Get the list of available contexts</td>
    <td style="padding:15px">{base_path}/{version}/JSON/context/view/contextList/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONContextViewContext(contextName, callback)</td>
    <td style="padding:15px">View context details</td>
    <td style="padding:15px">{base_path}/{version}/JSON/context/view/context/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONSessionManagementViewGetSessionManagementMethod(contextId, callback)</td>
    <td style="padding:15px">Gets the name of the session management method for the context with the given ID.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/sessionManagement/view/getSessionManagementMethod/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONSessionManagementViewGetSupportedSessionManagementMethods(callback)</td>
    <td style="padding:15px">Gets the name of the session management methods.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/sessionManagement/view/getSupportedSessionManagementMethods/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONSessionManagementActionSetSessionManagementMethod(contextId, methodName, callback)</td>
    <td style="padding:15px">Sets the session management method for the context with the given ID.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/sessionManagement/action/setSessionManagementMethod/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAuthenticationViewGetSupportedAuthenticationMethods(callback)</td>
    <td style="padding:15px">Gets the list of supported authentication methods.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/authentication/view/getSupportedAuthenticationMethods/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAuthenticationViewGetAuthenticationMethod(contextId, callback)</td>
    <td style="padding:15px">Gets the name of the authentication method for the context with the given ID.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/authentication/view/getAuthenticationMethod/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAuthenticationActionSetAuthenticationMethod(contextId, authMethodName, authMethodConfigParams, callback)</td>
    <td style="padding:15px">Sets the authentication method for the context with the given ID.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/authentication/action/setAuthenticationMethod/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAuthenticationActionSetLoggedInIndicator(contextId, loggedInIndicatorRegex, callback)</td>
    <td style="padding:15px">Sets the logged in indicator for the context with the given ID.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/authentication/action/setLoggedInIndicator/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAuthenticationActionSetLoggedOutIndicator(loggedOutIndicatorRegex, contextId, callback)</td>
    <td style="padding:15px">Sets Logged out indicator</td>
    <td style="padding:15px">{base_path}/{version}/JSON/authentication/action/setLoggedOutIndicator/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONUsersViewUsersList(contextId, callback)</td>
    <td style="padding:15px">Gets a list of users that belong to the context with the given ID, or all users if none provided.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/users/view/usersList/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONUsersViewGetUserById(contextId, userId, callback)</td>
    <td style="padding:15px">Gets the data of the user with the given ID that belongs to the context with the given ID.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/users/view/getUserById/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONUsersActionSetUserName(contextId, userId, name, callback)</td>
    <td style="padding:15px">Renames the user with the given ID that belongs to the context with the given ID.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/users/action/setUserName/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONUsersActionSetAuthenticationCredentials(contextId, userId, authCredentialsConfigParams, callback)</td>
    <td style="padding:15px">Sets the authentication credentials for the user with the given ID</td>
    <td style="padding:15px">{base_path}/{version}/JSON/users/action/setAuthenticationCredentials/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONUsersActionRemoveUser(contextId, userId, callback)</td>
    <td style="padding:15px">Removes the user with the given ID that belongs to the context with the given ID.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/users/action/removeUser/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONContextActionIncludeInContext(contextName, regex, callback)</td>
    <td style="padding:15px">Include regex to context</td>
    <td style="padding:15px">{base_path}/{version}/JSON/context/action/includeInContext/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONContextActionExcludeFromContext(contextName, regex, callback)</td>
    <td style="padding:15px">Include regex to context</td>
    <td style="padding:15px">{base_path}/{version}/JSON/context/action/excludeFromContext/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAscanActionScan(url, recurse, inScopeOnly, scanPolicyName, postData, contextId, callback)</td>
    <td style="padding:15px">Runs the active scanner</td>
    <td style="padding:15px">{base_path}/{version}/JSON/ascan/action/scan/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAscanActionAddScanPolicy(scanPolicyName, alertThreshold, attackStrength, callback)</td>
    <td style="padding:15px">Add scan policy</td>
    <td style="padding:15px">{base_path}/{version}/JSON/ascan/action/addScanPolicy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAscanActionUpdateScanPolicy(scanPolicyName, alertThreshold, attackStrength, callback)</td>
    <td style="padding:15px">Update scan policy</td>
    <td style="padding:15px">{base_path}/{version}/JSON/ascan/action/updateScanPolicy/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAscanViewScanPolicyNames(callback)</td>
    <td style="padding:15px">Scan policies names</td>
    <td style="padding:15px">{base_path}/{version}/JSON/ascan/view/scanPolicyNames/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAscanViewScanners(scanPolicyName, policyId, callback)</td>
    <td style="padding:15px">List of active scanners</td>
    <td style="padding:15px">{base_path}/{version}/JSON/ascan/view/scanners/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAscanViewPolicies(scanPolicyName, policyId, callback)</td>
    <td style="padding:15px">List of active policies</td>
    <td style="padding:15px">{base_path}/{version}/JSON/ascan/view/policies/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONPscanActionEnableAllScanners(callback)</td>
    <td style="padding:15px">Enable all the passive scanners</td>
    <td style="padding:15px">{base_path}/{version}/JSON/pscan/action/enableAllScanners/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONPscanActionDisableAllScanners(callback)</td>
    <td style="padding:15px">Disable all the passive scanners</td>
    <td style="padding:15px">{base_path}/{version}/JSON/pscan/action/disableAllScanners/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONPscanViewScanners(callback)</td>
    <td style="padding:15px">List pf passive scanners</td>
    <td style="padding:15px">{base_path}/{version}/JSON/pscan/view/scanners/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAlertViewAlert(id, callback)</td>
    <td style="padding:15px">Gets the alert with the given ID</td>
    <td style="padding:15px">{base_path}/{version}/JSON/alert/view/alert/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAlertViewAlerts(baseurl, start, count, riskId, callback)</td>
    <td style="padding:15px">Gets the alerts raised by ZAP, optionally filtering by URL or riskId, and paginating with 'start' position and 'count' of alerts</td>
    <td style="padding:15px">{base_path}/{version}/JSON/alert/view/alerts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAlertViewAlertsSummary(baseurl, callback)</td>
    <td style="padding:15px">Gets number of alerts grouped by each risk level, optionally filtering by URL</td>
    <td style="padding:15px">{base_path}/{version}/JSON/alert/view/alertsSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAlertViewAlertCountsByRisk(url, recurse, callback)</td>
    <td style="padding:15px">Gets a count of the alerts, optionally filtered as per alertsPerRisk</td>
    <td style="padding:15px">{base_path}/{version}/JSON/alert/view/alertCountsByRisk/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONStatsViewSiteStats(site, keyPrefix, callback)</td>
    <td style="padding:15px">Gets all of the global statistics, optionally filtered by a key prefix</td>
    <td style="padding:15px">{base_path}/{version}/JSON/stats/view/siteStats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONUsersActionNewUser(contextId, name, callback)</td>
    <td style="padding:15px">Creates a new user with the given name for the context with the given ID.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/users/action/newUser/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJSONAlertFilterViewAlertFilterList(contextId, callback)</td>
    <td style="padding:15px">Lists the alert filters of the context with the given ID.</td>
    <td style="padding:15px">{base_path}/{version}/JSON/alertFilter/view/alertFilterList/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
